mkdir /etc/snort-blocklist-poller 2> /dev/null
cp config/* /etc/snort-blocklist-poller/
cp scripts/blocklist_poller /usr/bin/
chmod +x /usr/bin/blocklist_poller
cp scripts/blocklist_poller_init.sh /usr/local/etc/rc.d/
chmod +x /usr/local/etc/rc.d/blocklist_poller_init.sh
echo "[OK] DONE"
/usr/local/etc/rc.d/blocklist_poller_init.sh
