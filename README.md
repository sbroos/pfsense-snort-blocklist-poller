Overview of the project
=======================


This is a script for use with the snort package on pfsense. Snort on pfsense has the option to block ip adresses that generate an alert using snort2c. These scripts monitor this blocklist. Whenever the list is changed, a predefined set of commands is executed. In the example configuration file a command is included that generates a sound whenever the blocklist changes [if you have a compatible device e.g; the ones sold by Electric Sheep Fencing LLC (c)].

# How to setup
* clone the repository to a temporary location
* from within the project directory, run the install.sh script "sh install.sh"

# How to use
* ssh to the pfsense firewall ip address (make sure you have ssh enabled)
* the snort package should be installed. Otherwise these scripts have no effect
* edit the file /etc/snort-blocklist-poller/commands and add commands that need to be executed whenever the blocklist changes
* multi line commands are currently not possible (maybe in a future version)
* if you want to execute multi line commands: write a script and put this in the commands config file